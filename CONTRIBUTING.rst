.. highlight:: shell

============
Contributing
============

Contributions are welcome, and they are greatly appreciated! Every little bit
helps, and credit will always be given.

You can contribute in many ways:

Types of Contributions
----------------------

Report Bugs
~~~~~~~~~~~

Report bugs at `Issue tracker <https://gitlab.com/fbisti/navarp/-/issues>`_.

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

Fix Bugs
~~~~~~~~

Look through the `Issue tracker <https://gitlab.com/fbisti/navarp/-/issues>`_ for bugs.
Anything tagged with "bug" and "help wanted" is open to whoever wants to implement it.

Implement Features
~~~~~~~~~~~~~~~~~~

Look through the `Issue tracker <https://gitlab.com/fbisti/navarp/-/issues>`_ for features.
Anything tagged with "enhancement" and "help wanted" is open to whoever wants to implement it.

Write Documentation
~~~~~~~~~~~~~~~~~~~

NavARP could always use more documentation, whether as part of the
official NavARP docs, in docstrings, or even on the web in blog posts,
articles, and such.

Submit Feedback
~~~~~~~~~~~~~~~

The best way to send feedback is to file an issue at `Issue tracker <https://gitlab.com/fbisti/navarp/-/issues>`_ .

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome :)

Get Started!
------------

Ready to contribute? Here's how to set up `navarp` for local development.

1. Create a dedicated conda environment (for example navarp-env)::

    $ conda create --name navarp-env numpy scipy matplotlib colorcet h5py pyqt=5 jupyter pyyaml click
    $ conda activate navarp-env
    $ conda install -c conda-forge igor git

2. Fork the `navarp` repo on GitLab.
3. Move in the folder where you want to have the package and clone your fork locally by running::

    $ git clone git@gitlab.com:your_name_here/navarp.git
	$ cd navarp/
	$ pip install -e .

4. Create a branch for local development::

    $ git checkout -b name-of-your-bugfix-or-feature

   Now you can make your changes locally and it will affect the navarp program as long as you stay in this enviroment (in this example is navarp-env).

5. When you're done making changes, check the code by using navarp and/or running the examples.

6. Commit your changes and push your branch to GitLab::

    $ git add .
    $ git commit -m "Your detailed description of your changes."
    $ git push origin name-of-your-bugfix-or-feature

7. Submit a pull request through the GitLab website.

Pull Request Guidelines
-----------------------

Before you submit a pull request, check that it meets these guidelines:

1. The pull request should include tests.
2. If the pull request adds functionality, the docs should be updated. Put
   your new functionality into a function with a docstring, and add the
   feature to the list in README.rst.

Deploying
---------

A reminder for the maintainers on how to deploy.
Make sure all your changes are committed (including an entry in CHANGELOG.rst).
Then run::

$ bump2version patch # possible: major / minor / patch
$ git push
$ git push --tags
